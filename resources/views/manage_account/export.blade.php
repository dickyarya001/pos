<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Account</title>

    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <p style="font-weight:bold; font-size: 20px; text-align:left;">Daftar Akun</p>
    <table cellspacing="0" id="myTable">
        <thead>
            <tr>
                <th scope="col">Nama Akun</th>
                <th scope="col">Email</th>
                <th scope="col">Posisi</th>
                <th scope="col">Admin Input</th>
                <th scope="col">Tanggal Diinput</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>
                        {{ $user->firstname}} {{ $user->lastname }}
                    </td >
                    <td>{{ $user->email }}</td>
                    <td>
                        @if($user->user_position == "superadmin_pabrik")
                            superadmin
                        @elseif($user->user_position == "superadmin_distributor")
                            distributor
                        @else
                            {{ $user->user_position }}
                        @endif    
                    </td>
                    @canany(['superadmin_pabrik','admin'])
                        <td>
                            @if($admins->where('id', $user->id_input)->first())
                            {{ $admins->where('id', $user->id_input)->first()->firstname }} {{ $admins->where('id', $user->id_input)->first()->lastname }}
                            @else
                            {{ $user->nama_input }}
                            @endif
                        </td>
                    @endcan
                    @can('superadmin_distributor')
                        <td>
                            {{ auth()->user()->firstname }} {{ auth()->user()->lastname }}
                        </td>
                    @endcan
                    <td>{{ $user->created_at->format('d/m/y H:i:s') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <table>
        @canany(['superadmin_pabrik', 'admin'])
            <tr>
                <td>Total Akun</td>
                <td>:</td>
                <td>{{ $countTotal }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Super Admin Pabrik</td>
                <td>:</td>
                <td>{{ $countSuperAdminPabrik }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Admin Pabrik</td>
                <td>:</td>
                <td>{{ $countAdmin }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Accounting Pabrik</td>
                <td>:</td>
                <td>{{ $countAccountingPabrik }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Cashier Pabrik</td>
                <td>:</td>
                <td>{{ $countCashierPabrik }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Distributor</td>
                <td>:</td>
                <td>{{ $countSuperAdminDistributor }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Prospek Distributor</td>
                <td>:</td>
                <td>{{ $countProspekDistributor }} Akun</td>
            </tr>
        @endcan
        @can('superadmin_distributor')
            <tr>
                <td>Total Akun</td>
                <td>:</td>
                <td>{{ $countTotal }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Distributor</td>
                <td>:</td>
                <td>{{ $countSuperAdminDistributor }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Accounting Distributor</td>
                <td>:</td>
                <td>{{ $countAccountingDistributor }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Cashier Distributor</td>
                <td>:</td>
                <td>{{ $countCashierDistributor }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Reseller</td>
                <td>:</td>
                <td>{{ $countReseller }} Akun</td>
            </tr>
            <tr>
                <td>Total Akun Prospek Reseller</td>
                <td>:</td>
                <td>{{ $countProspekReseller }} Akun</td>
            </tr>
        @endcan
    </table>
</body>
</html>