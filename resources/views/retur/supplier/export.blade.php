<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Account</title>

    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <p style="font-weight:bold; font-size: 20px; text-align:left;">
        @can('superadmin_pabrik')
        Retur Pabrik ke Supplier
        @endcan
        @can('superadmin_distributor')
        Retur Distributor ke Pabrik
        @endcan
        @can('reseller')
        Retur Reseller ke Distributor
        @endcan
    </p>
    <br>
    <table id="myTable" class="table table-hover table-striped table-light text-left" >
        <thead>
            <tr id="_judul" onkeyup="_filter()" id="myFilter">
                <th scope="col">No Nota</th>
                <th scope="col">Supplier</th>
                <th scope="col">No Surat Keluar</th>
                <th scope="col">Tanggal Retur</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Status</th>
                <th scope="col">Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach($returs as $retur)
                <tr>
                    @if(auth()->user()->user_position == "superadmin_pabrik")
                    <td scope="col">{{ $retur->pasok->kode_pasok }}</td>
                    @else
                    <td scope="col">{{ $retur->transaction->transaction_code }}</td>
                    @endif
                    <td scope="col">
                        @if($retur->id_supplier == 0)
                        Pabrik Astana
                        @else
                        {{ $retur->supplier->firstname }} {{ $retur->supplier->lastname }} 
                        @endif
                    </td>
                    <td scope="col">{{ $retur->surat_keluar }}</td>
                    <td scope="col">{{ $retur->updated_at->format('d/m/y H:i:s') }}</td>
                    <td scope="col">{{ $retur->keterangan }}</td>
                    <td scope="col">
                        @if($retur->status_retur == 0)
                            Menunggu Konfirmasi
                        @else
                            Retur Sukses
                        @endif
                    </td>
                    <td scope="col">Rp. {{ number_format($retur->total, 0, ',', '.') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>