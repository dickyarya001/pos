@extends('templates/main')

@section('title')
    <h1 class="m-0">List Distributor</h1>
@endsection

@section('content')
<div class="container">
    <div class="col-lg-12">
        <div class="d-flex">
            <div class="container">
                <!-- <div class="row d-flex justify-content-between"> -->
                <div class="row d-flex ">
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-primary" onclick="window.location.href='{{ url('/sales/list_distributor/export') }}'">
                            <i class="fa fa-file-pdf-o"></i>
                            <span>Export</span></button>
                        <button type='button' class='btn btn-primary' onclick="window.open('{{ url('/sales/list_distributor/print') }}')">
                        <i class='fa fa-print'></i>Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="iq-card">
        <div class="iq-card-body">
            <table id="mytable" class="table table-hover table-striped table-light" style="text-align:left">
                <thead>
                    <tr>
                        <th scope="col">ID Distributor</th>
                        <th scope="col">Nama Distributor</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($distributors as $distributor)
                        <tr>
                            <td>{{ $distributor->id }}</td>
                            <td>{{ $distributor->firstname }} {{ $distributor->lastname }}</td>
                            <td><button class="btn btn-primary"
                                    onclick="location.href='{{ url('/sales/index/'.$distributor->id_group) }}'"><i
                                        class="fa fa-eye"></i></button></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function () {
    $('#mytable').DataTable(
        {
        "oSearch": { "bSmart": false, "bRegex": true },
        }
    );
});
</script>
@endsection