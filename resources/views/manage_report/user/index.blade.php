@extends('templates/main')

@section('title')
<h1 class="m-0">Laporan Pegawai</h1>
@endsection

@section('content')
<div class="container">
    <div class="col-lg-12">
        <div class="d-flex">
            <div class="container">
                <div class="row ">

                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary"
                            onclick="location.href='{{ url('/manage_report/users/export') }}'">
                            <i class="fa fa-file-pdf-o"></i>
                            <span>Export</span>
                        </button>
                        <button type='button' class='btn btn-primary'
                            onclick="window.open('{{ url('/manage_report/users/print') }}')"><i class='fa fa-print'></i>
                            Print</button>
                    </div>

                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>

    <hr>
    <div class="iq-card">
        <div class="iq-card-body">
            <table id="mytable" class="table table-responsive table-hover table-striped table-light text-nowrap"
                style="text-align:left" id="myTable">
                <thead>
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Email</th>
                        <th scope="col">Posisi</th>
                        <th scope="col">Aktifitas Pembelian</th>
                        <th scope="col">Aktifitas Penjualan</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pegawais as $pegawai)
                    <tr>
                        <td class='jname full-body'>
                            @if($pegawai->image)
                            <img src={{ asset('storage/' . $pegawai->image) }} alt=profile-img
                                class="avatar-50 roundimg" img-fluid />
                            @else
                            <img src={{ asset('images/manage_account/users/11.png') }} alt=profile-img
                                class="avatar-50 roundimg" height="35px" width="35px" img-fluid />
                            @endif
                            {{ $pegawai->firstname }} {{ $pegawai->lastname }}
                        </td>
                        <td>{{ $pegawai->email }}</td>
                        <td>{{ $pegawai->user_position }}</td>
                        @if(auth()->user()->id_group == 1)
                        <td>
                            @php
                            $countPembelian = \App\Models\SupplyHistory::where('id_input',$pegawai->id)->count();
                            @endphp
                            {{ $countPembelian }} x
                        </td>
                        <td>
                            @php
                            $countPenjualan = \App\Models\TransactionHistory::where('id_approve',$pegawai->id)->count();
                            @endphp
                            {{ $countPenjualan }} x
                        </td>
                        @else
                        <td>
                            @php
                            $countPembelian = \App\Models\TransactionHistory::where('id_input',$pegawai->id)->count();
                            @endphp
                            {{ $countPembelian }} x
                        </td>
                        <td>
                            @php
                            $countPenjualan = \App\Models\TransactionHistory::where('id_approve',$pegawai->id)->count();
                            $countPenjualanKasir =
                            \App\Models\TransactionHistorySell::where('id_input',$pegawai->id)->count();
                            $countPenjualanTracking =
                            \App\Models\TrackingSalesHistory::where('id_reseller',$pegawai->id)->count();
                            @endphp
                            {{ $countPenjualan+$countPenjualanKasir+$countPenjualanTracking }} x
                        </td>
                        @endif
                        <td class="col-1">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"
                                    onclick="location.href='{{ url('/manage_report/users/detail/'.$pegawai->id) }}'">
                                    <i class="fas fa-eye"></i>&nbspLihat Detail</button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#mytable').DataTable({
            "oSearch": {
                "bSmart": false,
                "bRegex": true
            },
        });
    });

</script>
@endsection
