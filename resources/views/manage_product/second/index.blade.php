@extends('templates/main')

@section('title')
    <h1 class="m-0">
    @if(auth()->user()->id_group == 1)
    Daftar Barang Distributor
    @else
    Daftar Barang Reseller
    @endif</h1>
@endsection

@section('content')
<div class="container-fluid">
    <div class="col-12">
        <div class="mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-between">
                        <div class="col-sm-2" style="text-align:left;">
                            @if(auth()->user()->id_group == 1)
                                <button type="button" class="btn btn-primary" onclick="window.location.href='{{ url('/manage_product/distributor/export/') }}'">
                                    <i class="fa fa-file-pdf-o"></i>
                                    <span>Export</span></button>
                                <button type='button' class='btn btn-primary' onclick="window.open('{{ url('/manage_product/distributor/print/') }}')">
                                    <i class='fa fa-print'></i>Print</button>
                            @else
                                <button type="button" class="btn btn-primary" onclick="window.location.href='{{ url('/manage_product/reseller/export/') }}'">
                                    <i class="fa fa-file-pdf-o"></i>
                                    <span>Export</span></button>
                                <button type='button' class='btn btn-primary' onclick="window.open('{{ url('/manage_product/reseller/print/') }}')">
                                    <i class='fa fa-print'></i>Print</button>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="row d-flex justify-content-center align-items-center">  
                        <div class="col-12 grid-margin ">
                            <div class="iq-card">
                                <div class="iq-card-body">
                                    <div class="row">
                                    </div>
                                    <div class="table-responsive-xl" style="overflow: scroll; ">  
                                        <table class="table table-hover table-striped table-light display sortable  text-nowrap" cellspacing="0"  id="myTable" >
                                            <thead>
                                                <br>
                                                <tr id="_judul" onkeyup="_filter()" id="myFilter">
                                                    <th >ID</th>
                                                    @if(auth()->user()->id_group == 1)
                                                    <th>Distributor</th>
                                                    @else
                                                    <th>Reseller</th>
                                                    @endif
                                                    <th >Aksi</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($lists as $list)
                                                    <tr>
                                                        <td>{{ $list->id }}</td>
                                                        <td>{{ $list->firstname }} {{ $list->lastname }}</td>
                                                        <td>
                                                            @if(auth()->user()->id_group == 1)
                                                            <button class="btn btn-primary btn-sm" onclick="location.href='{{ url('/manage_product/distributor/products/'.$list->id) }}'">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                            @else
                                                            <button class="btn btn-primary btn-sm" onclick="location.href='{{ url('/manage_product/reseller/products/'.$list->id) }}'">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){
    $('#myTable').DataTable(
        {
        "oSearch": { "bSmart": false, "bRegex": true },
        }
    );
});

</script>
@endsection