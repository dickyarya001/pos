@extends('templates/main')

@section('title')
    <h1 class="m-0">Daftar Barang Reseller</h1>
@endsection

@section('content')
<div class="container">
    <div class="col-12">
        <div class="mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-between">
                        <div class="container">
                            <div class="row d-flex align-items-center">
                                <div class="col-sm" style="text-align:left;">
                                    <button type="button" class="btn btn-primary" onclick="window.location.href='{{ url('/manage_product/distributor_reseller/export/') }}'">
                                        <i class="fa fa-file-pdf-o"></i>
                                        <span>Export</span>
                                    </button>
                                    <button type='button' class='btn btn-primary' onclick="window.open('{{ url('/manage_product/distributor_reseller/print/') }}')">
                                        <i class='fa fa-print'></i>Print</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    
                    <div class="row d-flex justify-content-center align-items-center">  
                        <div class="col-12 grid-margin ">
                            <div class="iq-card">
                                    <div class="iq-card-body">
                                        <div class="table-responsive-xl" style="overflow: scroll; ">  
                                            <table class="table table-hover table-striped table-light display sortable  text-nowrap" cellspacing="0"  id="myTable" >
                                                <thead>
                                                    <br>
                                                    <tr id="_judul" onkeyup="_filter()" id="myFilter">
                                                        <th >ID</th>
                                                        <th >Distributor</th>
                                                        <th >Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($distributors as $distributor)
                                                        <tr>
                                                            <td>{{ $distributor->id }}</td>
                                                            <td>{{ $distributor->firstname }} {{ $distributor->lastname }}</td>
                                                            <td><button class="btn btn-primary btn-sm" onclick="location.href='{{ url('/manage_product/distributor_reseller/products/'. $distributor->id) }}'"><i class="fa fa-eye"></button></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>

$(document).ready(function(){
    $('#myTable').DataTable(
        {
        "oSearch": { "bSmart": false, "bRegex": true },
        }
    );
});
</script>
@endsection